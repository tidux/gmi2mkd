# Test Gemini Article

This is a paragraph.

```
$ echo "This is a code block."
```

=> gemini://gemini.circumlunar.space This is a Gemini protocol link.
=> https://www.youtube.com/watch?v=dQw4w9WgXcQ This is a Rickroll.
=> https://tildegit.org/sloum/gemini-vim-syntax This is Vim syntax highlighting for Gemini files.
