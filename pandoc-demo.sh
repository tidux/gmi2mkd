#!/bin/bash
set -eo pipefail
echo "This is a demonstration of how to use gmi2mkd as a filter for producing HTML output from Gemini sources."
echo "This script will fail if you do not have pandoc or Firefox on \$PATH.  This is intended behavior."
cat example.gmi | gmi2mkd | pandoc -f markdown -t html -o example.html
firefox example.html
